%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SDP code to calculate secure key fraction for arbitary dimension and arbitrary number of monitoring states
%Authors: Nurul T. Islam, Charles Ci Wen Lim and Daniel J. Gauthier
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear
% 
SKR = [];
PError = [];
EError = [];

d = 4 %set dimension, as an example we set it to 4
s = 1; %set the number of states, as an example we set to 1
m = d*d;

%% Binary entropy definition
%cvx_setup
h2=@(x)-x*log2(x/(d-1))-(1-x)*log2(1-x);

%define z matrix
z = eye(d);


%% Operators
%Define Z(i) = |t_n><t_n|
for i = 1:d
    Z{i} = kron(z(:,i),transpose(conj(z(:,i))));
end

%Define the fft and X(i) = |f_n><f_n|
x = fft(z);
for i = 1:d
    X{i} = 1/d*(kron(x(:,i),transpose(conj(x(:,i)))));
end

%Calculating the bit_error matrix and phase_error matrix

BitErr = eye(m);
for i = 1:d+1:m
    BitErr(i,i) = 0;
end

PhaseErr = 1/(m)*kron(transpose(conj(fft(z))),fft(z))*BitErr*kron(fft(z),transpose(conj(fft(z))));


%% Measurements
%Partial measurements
for i = 1:d
    for j = 1:d
        ZX{i}{j} = kron(Z{i},X{j});
        XZ{i}{j} = kron(conj(X{i}),Z{j});
        XX{i}{j} = kron(conj(X{i}),X{j});
    end
end

%% fixed experimental parameters and initializations
reso=1;
Expt_Err=linspace(0.02664155527,0.02664155527,reso); %% The two numbers 0.02664155527 indicate an example QBER. 
%%If you want to calculate the SKR over a range of QBER, define the number
%%of points using reso, and include
%%Expt_Err=linspace(lower_limit_qber,upper_limit_qber,reso

rate=zeros(1,reso);
max_phase=zeros(1,reso);



%% the parameter n defines the resolution of qber as defined above
for n=1:1;
    n
    % Begins SDP optimization
    cvx_begin sdp quiet

    % Declare general two-qudit bipartite state
    variable R(m,m) hermitian semidefinite;

    % Objective
    Max_Phase=0;
    Max_Phase= real(trace(PhaseErr*R)); %It needs to be real because matlab
    % makes the imag part like 0.0000i so the program thinks it's imaginary

    maximize Max_Phase;
    subject to 
    trace(R)==1;
    R >= 0;
    %trace of BitErr*R
    trace(sparse(BitErr*R)) == sparse(Expt_Err(n));
    
    %Partial measurements
    for i = 1:1:d
        for j = 1:1:d
            trace(sparse(ZX{i}{j}*R)) == 1/m;
        end
    end
    %For number of MUBs not equal to dimension, they are different
    for i = 1:1:s
        for j = 1:1:d
            trace(sparse(XZ{i}{j}*R)) == 1/m;
            if i == j
                trace(sparse(XX{i}{j}*R)) == (1-Expt_Err(n))/d;
            else
                trace(sparse(XX{i}{j}*R)) == Expt_Err(n)/(d*(d-1));
            end
        end
    end
    cvx_end

    max_phase(n)=cvx_optval;
    max_phase(1)
    rate(n)=(log2(d)-h2(Expt_Err(n))-h2(max_phase(n)));
    rate(n)
    end

SKR = [SKR; rate];
PError = [PError; max_phase];
EError = [EError; Expt_Err];
plot(Expt_Err,max(rate,0))





















